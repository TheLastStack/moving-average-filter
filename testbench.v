`timescale 1ns / 1ps
//Verilog testbench to verify the functionality of the address generation module
module testbench;
reg clk, reset;
reg[2:0] in_row, in_col;
wire address_enable, op_enable, op, op_reset;
wire[2:0] row_out, col_out;
AddressGenerator #(.END_R(5),.END_C(5),.FILTER_SIZE(1)) AdGen (.clk(clk), .reset(reset), .in_row(in_row), .in_col(in_col), .address_enable(address_enable), .op_enable(op_enable), .op(op), .op_reset(op_reset), .row_address(row_out), .col_address(col_out));
always #3 clk=clk+1;
initial begin
	$dumpfile("my_newdumpfile.vcd");
	$dumpvars(0,testbench);
	reset = 0;
	clk = 0;
	in_row = 3;
	in_col = 4;
	#2 reset = 1;
	#7 reset = 0;
	#200 $finish;
	end
endmodule
