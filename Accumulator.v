`timescale 1ns / 1ps
`define toint(args) $rtoi($floor($log10(args)/$log10(2)))
//Asynchronous reset called by main module
//Synchronous reset called by address generator. Clears the sum when called
module Accumulator #(parameter FILTER_SIZE = 1) (input clk, input reset, input op, input op_enable, input op_reset, input[7:0] data, output[`toint((2 * FILTER_SIZE + 1) ** 2)+8:0]sum);
reg[`toint((2 * FILTER_SIZE + 1) ** 2)+8:0] store;
assign sum=store;
always@(posedge clk or posedge reset)
begin
    if(reset)
    store = 0;
    else begin
        if (op_reset) begin
            store = 0;
            end
        else begin
            if(op_enable) begin
                case(op)
                    1'b0: store = store + data;
                    1'b1: store = store - data;
                endcase
                end
            end
        end
    end
endmodule
