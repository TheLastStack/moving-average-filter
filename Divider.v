`timescale 1ns / 1ps
`define toint(args) $rtoi($floor($log10(args)/$log10(2)))

//Performs division on the accumulated data
module Divider #(parameter FILTER_SIZE = 1) (input[`toint((2 * FILTER_SIZE + 1) ** 2)+8:0]in_data, output[7:0]out_data);
reg[`toint((2 * FILTER_SIZE + 1) ** 2):0] divisor;
reg[`toint((2 * FILTER_SIZE + 1) ** 2)+8:0] dividend;
reg[7:0] quotient;
integer i;
assign out_data = quotient; 
generate
always@*
begin
    divisor = (2 * FILTER_SIZE + 1) ** 2;
    dividend = in_data;
    quotient = 0;
    for(i=7; i>=0; i=i-1)
    begin
        if(dividend[i+:`toint((2 * FILTER_SIZE + 1) ** 2) + 2] >= divisor)
        begin
            dividend[i+:`toint((2 * FILTER_SIZE + 1) ** 2) + 2] = dividend[i+:`toint((2 * FILTER_SIZE + 1) ** 2) + 2] - divisor;
            quotient[i] = 1'b1;
        end
    end
end
endgenerate
endmodule