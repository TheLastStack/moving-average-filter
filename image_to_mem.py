#This script is used to convert a grayscale image to a series of bits for input
import cv2
import numpy as np
img = cv2.imread('Cameraman.png')
gray, _, _ = cv2.split(img) #Note that this is true for greyscale images only.
flat_gray = gray.flatten()
file_string = ''
for i in flat_gray:
    file_string += '{:0>2X}\n'.format(i) # {:0>2X} converts the number from decimal to hexadecimal with zeropadding till the number of digits is set to 2
file_string = file_string[:-1]
with open('Image_data.bit', 'w') as f:
    f.write(file_string)
print("Dimensions of the image is " + str(gray.shape))
