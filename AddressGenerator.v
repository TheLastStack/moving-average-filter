`timescale 1ns / 1ps
`define check_bounds(ad, l, r) (ad >= l && ad < r)
`define toint(args) $rtoi($floor($log10(args)/$log10(2)))
`define invalid op_enable_1 <= 0;
`define valid op_enable_1 <= 1;
module AddressGenerator #(parameter END_R = 240,parameter END_C = 240,parameter FILTER_SIZE = 1) (input clk, input reset, input[`toint(END_R):0] in_row, input[`toint(END_C):0] in_col, output reg address_enable, output op_enable, output reg op, output reg op_reset, output reg[`toint(END_R):0] row_address, output reg[`toint(END_C):0] col_address);
reg row_enable, row_reset, col_enable, col_reset, mode, op_enable_1, op_enable_2;
reg[`toint((2 * FILTER_SIZE + 1)):0] col_limit;
wire[`toint((2 * FILTER_SIZE + 1)):0] row_count, col_count, row_limit;
wire row_done, col_done;
localparam COMP_ROW = (`toint((2 * FILTER_SIZE + 1)) > `toint(END_R) ? `toint(2 * FILTER_SIZE + 1) : `toint(END_R)) + 1;
localparam COMP_COL = (`toint((2 * FILTER_SIZE + 1)) > `toint(END_C) ? `toint(2 * FILTER_SIZE + 1) : `toint(END_C)) + 1;
wire signed [COMP_ROW:0]reference_row;
wire signed [COMP_COL:0]reference_col;
wire signed [COMP_ROW:0]signed_row_count;
wire signed [COMP_COL:0]signed_col_count;
wire signed [COMP_ROW:0]row_filter_size = FILTER_SIZE;
wire signed [COMP_COL:0]col_filter_size = FILTER_SIZE;
assign row_limit = 2 * FILTER_SIZE + 1;
Counter #(.until((2 * FILTER_SIZE + 1))) row_counter(.clk(clk), .reset(row_reset), .enable(row_enable), .limit(row_limit), .count(row_count), .done(row_done));
Counter #(.until((2 * FILTER_SIZE + 1))) col_counter(.clk(row_done), .reset(col_reset), .enable(col_enable), .limit(col_limit), .count(col_count), .done(col_done));
assign reference_row = in_row;
assign reference_col = in_col;
assign signed_row_count = row_count;
assign signed_col_count = col_count;
assign op_enable = op_enable_1 && op_enable_2;
always@(posedge clk or posedge reset) begin
    if(reset) begin
        row_enable <= 0;
        row_reset <= 1;
        col_enable <= 0;
        col_reset <= 1;
        op_enable_2 <= 0;
        address_enable <= 0;
        row_address <= 0;
        col_address <= 0;
        mode <= 1;
    	col_limit <= 2 * FILTER_SIZE + 1;
        op_reset <= 0;
        end
    else begin
        if (op_reset) begin
            op_reset <= 0;
            end
        else begin
            if(address_enable) begin
                address_enable <= 0;
                op_enable_2 <= 0;
                row_enable <= 0;
                row_reset <= 1;
                col_enable <= 0;
                col_reset <= 1;
                row_address <= 0;
                col_address <= 0;
                if(in_col == END_C - 1) begin
                    op_reset <= 1;
                    end
                end
            else begin
                if (col_count == col_limit - 1 && row_count == 2 * FILTER_SIZE) begin
                    address_enable <= 1;
                    row_enable <= 0;
                    col_enable <= 0;
                    row_reset <= 1;
                    col_reset <= 1;
                    op_enable_2 <= 0;
                    end
                else begin
                    row_enable <= 1;
                    col_enable <= 1;
                    col_reset <= 0;
                    row_reset <= 0;
                    op_enable_2 <= 1;
                    end
                if (in_col == 0) begin
                    col_limit <= 2 * FILTER_SIZE + 1;
                    mode <= 1;
                    end
                else begin
                    col_limit <= 2;
                    mode <= 0;
                    end
                end
            end
        end
    end
always@* begin
    if(`check_bounds(reference_row + signed_row_count - row_filter_size, 0, END_R)) begin
        if(mode) begin
            op <= 0;
            if(`check_bounds(reference_col + signed_col_count - col_filter_size, 0, END_C)) begin
                `valid
                row_address <= reference_row + signed_row_count - row_filter_size;
                col_address <= reference_col + signed_col_count - col_filter_size;
                end
            else begin
                `invalid
                end
            end
        else begin
            if(col_count == 0) begin
                op <= 1;
                if(`check_bounds(reference_col - FILTER_SIZE - 1, 0, END_C)) begin
                    `valid
                    row_address <= reference_row + signed_row_count - row_filter_size;
                    col_address <= reference_col - col_filter_size - 1;
                    end
                else begin
                    `invalid
                    end
            end
            else begin
                op <= 0;
                if(`check_bounds(reference_col + FILTER_SIZE, 0, END_C)) begin
                    `valid
                    row_address <= reference_row + signed_row_count - row_filter_size;
                    col_address <= reference_col + col_filter_size;
                    end
                else begin
                    `invalid
                    end
                end
            end
        end
    else begin
        `invalid
        end
    end
endmodule
