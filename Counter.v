`timescale 1ns / 1ps
`define toint(args) $rtoi($floor($log10(args)/$log10(2)))

//Counter counts till limit.
// Parameter until is used to set the largest possible limit
module Counter #(parameter until=20)(input clk, input reset, input enable, input[`toint(until):0]limit, output reg done, output[`toint(until):0] count);
reg[`toint(until):0] internal;
assign count=internal;
always@(posedge clk or posedge reset)
begin
    if(reset) begin
        internal <= 0;
        done <= 0;
        end
    else begin
        if(enable) begin
            if(internal < limit - 1) begin
                internal <= internal + 1;
                done <= 0;
                end
            else begin
                internal <= 0;
                done <= 1;
                end
            end
        end
    end
endmodule