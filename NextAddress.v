`timescale 1ns / 1ps
`define toint(args) $rtoi($floor($log10(args)/$log10(2)))
// Calculates the next address
module NextAddress #(parameter END_R = 240, parameter END_C = 240)(input[`toint(END_R):0] row_address_in, input[`toint(END_C):0] col_address_in, output reg[`toint(END_R):0] row_address_out, output reg[`toint(END_C):0] col_address_out);
// Combinational circuit
always@*
begin
    if(col_address_in >= END_C - 1)
        begin
            row_address_out <= row_address_in + 1;
            col_address_out <= 0;
        end
    else
        begin
            col_address_out <= col_address_in + 1;
            row_address_out <= row_address_in;
        end    
end
endmodule