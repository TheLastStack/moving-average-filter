`timescale 1ns / 1ps
//testbench for the finished module
module FilterImage;
reg clk, reset;
wire done;
MAV #(.END_R(256), .END_C(256), .FILTER_SIZE(1)) filter (.clk(clk), .in_reset(reset), .done(done));
always #3 clk = clk + 1;
initial begin
    $dumpfile("debug.vcd");
	$dumpvars(0,FilterImage);
    clk = 0;
    reset = 0;
    #1 reset = 1;
    #5 reset = 0;
    #10000000 $finish;
    end
endmodule