`timescale 1ns / 1ps
`define toint(args) $rtoi($floor($log10(args)/$log10(2)))
//Module for wiring all other modules together. 
module MAV #(parameter END_R=256, parameter END_C=256, parameter FILTER_SIZE=1) (input clk, input in_reset, output done);
wire[`toint(END_R):0]current_ref_row, next_ref_row, mem_row; 
//Current row address being filled in write-only memory, next row to fill, and current row address used for reading from read only memory
wire[`toint(END_C):0]current_ref_col, next_ref_col, mem_col; 
//Similar to above
wire address_shift, accumulator_enable, operation_choice, accumulator_reset, reset; 
//Signals the address register to move to the next address
//accumulator enable enables an operation on the accumulator
//operation choise chooses between subtraction and addition on the accumulator
//accumulator reset is a special synchronous reset used to reset the sum to 0 at the start of a new row of pixels
// reset is a general reset pin for most modules
wire[7:0] read_data, write_data;
//read_data is the data read from the read-only memory
//write_data is the data read from the write-only memory
wire[`toint((2 * FILTER_SIZE + 1) ** 2)+8:0] sum;
//This is the current sum held by the acumulator
assign reset = in_reset || done;
//Reset pin is set to the input reset. Special case reset when the program is completed

//Wiring
AddressRegister #(.END_R(END_R), .END_C(END_C)) adreg (.clk(clk), .reset(in_reset), .enable(address_shift), .row_address_in(next_ref_row), .col_address_in(next_ref_col), .done(done), .row_address_out(current_ref_row), .col_address_out(current_ref_col));
NextAddress #(.END_R(END_R), .END_C(END_C)) getnext (.row_address_in(current_ref_row), .col_address_in(current_ref_col), .row_address_out(next_ref_row), .col_address_out(next_ref_col));
AddressGenerator #(.END_R(END_R), .END_C(END_C), .FILTER_SIZE(FILTER_SIZE)) adgen (.clk(clk), .reset(reset), .in_row(current_ref_row), .in_col(current_ref_col), .address_enable(address_shift), .op_enable(accumulator_enable), .op(operation_choice), .op_reset(accumulator_reset), .row_address(mem_row), .col_address(mem_col));
Accumulator #(.FILTER_SIZE(FILTER_SIZE)) addsub (.clk(clk), .reset(reset), .op(operation_choice), .op_enable(accumulator_enable), .op_reset(accumulator_reset), .data(read_data), .sum(sum));
Divider #(.FILTER_SIZE(FILTER_SIZE)) div (.in_data(sum), .out_data(write_data));
ReadOnlyDataMemory #(.END_R(END_R), .END_C(END_C)) original_image (.reset(reset), .row_address(mem_row), .col_address(mem_col), .mem_value(read_data));
WriteOnlyDataMemory #(.END_R(END_R), .END_C(END_C)) mav_image (.reset(reset), .write_en(address_shift), .row_address(current_ref_row), .col_address(current_ref_col), .mem_value(write_data), .done(done));
endmodule