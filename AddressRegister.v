`timescale 1ns / 1ps
`define toint(args) $rtoi($floor($log10(args)/$log10(2)))

//Stores address of pixel whose value is currently being calculated
//Also knows when the program is done and signals it.
module AddressRegister #(parameter END_R = 240, parameter END_C = 240)(input reset, input enable, input clk, input[`toint(END_R):0]row_address_in, input[`toint(END_C):0]col_address_in, output reg done, output reg[`toint(END_R):0]row_address_out, output reg[`toint(END_C):0]col_address_out);
always@(posedge clk or posedge reset) begin
    if(reset) begin
        row_address_out <= 0;
        col_address_out <= 0;
        done <= 0;
        end
    else begin
        if(enable) begin
            row_address_out <= row_address_in;
            col_address_out <= col_address_in;
            end
        end
    end
always@* begin
    if(row_address_out == END_R && col_address_out == 0) begin
        done <= 1;
        end
    end
endmodule
