# This python script is used to convert the output bits to the finished image
import cv2
import os
import numpy as np
with open('MAV_Image_data.bit', 'r') as f:
    filestring = f.read()
# Verilog's $writememh leaves comments with the address at periodic intervals. It also ends the file with a new line. Reject all of those.
array = np.array([int(i, 16) for i in filestring.split('\n') if (not i.startswith('/') and not i.startswith('x') and not i == '')])
satisfy = True
while satisfy:
    img_height = int(input("Enter image height:"))
    img_width = int(input("Enter image width:"))
    if img_height * img_width == array.shape[0]:
        satisfy = False
img = np.zeros([img_height, img_width, 3])
gray = array.reshape((img_height, img_width))
for i in range(0, 3):
    img[:, :, i] = gray
cv2.imwrite(os.path.join(os.getcwd(), "Result.png"), img)