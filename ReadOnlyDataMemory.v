`timescale 1ns / 1ps
//END_C is a parameter for the height of the image
//END_R is a parameter for the width of the image
//Reset is used to load the image
//mem_address is the address bus
//mem_value outputs the data
`define toint(args) $rtoi($floor($log10(args)/$log10(2)))
module ReadOnlyDataMemory #(parameter END_R = 240, parameter END_C = 240) (input reset, input[`toint(END_R):0]row_address, input[`toint(END_C):0]col_address, output[7:0] mem_value);
reg[7:0] memory[END_R * END_C - 1:0];
wire[`toint(END_R * END_C - 1):0] expanded_bus_row, expanded_bus_col;
genvar m;
generate
    for(m=0; m<=`toint(END_R*END_C - 1); m=m+1) 
    begin
        if(m<=`toint(END_R))
            assign expanded_bus_row[m] = row_address[m];
        else begin
            assign expanded_bus_row[m] = 0;
            end
    end
endgenerate

genvar n;
generate
    for(n=0; n<=`toint(END_R*END_C - 1); n=n+1)
    begin
        if(n<=`toint(END_R))
            assign expanded_bus_col[n] = col_address[n];
        else
            assign expanded_bus_col[n] = 0;
    end
endgenerate
assign mem_value = memory[expanded_bus_row * END_C + expanded_bus_col];
always@(posedge reset)
begin
    $readmemh("Image_data.bit", memory);
end
endmodule
